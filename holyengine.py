import argparse
import functools
import json
import re
import ssl
import tempfile
import textwrap
import urllib
import urllib.request
from datetime import datetime
from pathlib import Path

import call_to_dxcc
from cabrillo.data import FREQ_RANGES
from cabrillo.parser import parse_log_text

ssl._create_default_https_context = ssl._create_unverified_context


HOLYSQUARE_REGEX = re.compile(r"[A-Z]\d{2}[A-Z]{2}")
current_year = datetime.now().year


class InvalidLog(Exception):
    pass


class Holylog:
    _ERROR_FIXES = {
        "START-OF-LOG: 2.0": "START-OF-LOG: 3.0",
        "CATEGORY-OVERLAY: CHECKLOG\r\n": "",
        r"CLAIMED-SCORE:\s*\r\n": "",
        "CATEGORY-ASSISTED: (NO ASSISTED|NONASSISTED)": "CATEGORY-ASSISTED: NON-ASSISTED",
        r"CATEGORY-MODE:\s*(MIX|MM)\r\n": "CATEGORY-MODE: MIXED\r\n",
        "CATEGORY-BAND: (ALL BAND|ALL)\r\n": "CATEGORY-MODE: MIXED\r\n",
        "CATEGORY-BAND: All-Bands": "CATEGORY-BAND: ALL",
        "CATEGORY-BAND: 20M-15M-10M": "CATEGORY-BAND: ALL",
        r"CATEGORY-BAND:\s*(\d+)(\s*M)?": r"CATEGORY-BAND: \1M",
        "CATEGORY-TRANSMITTER: MULTI\r\n": "CATEGORY-TRANSMITTER: UNLIMITED\r\n",
        "CATEGORY-STATION: DISTRIBUTED\r\n": "CATEGORY-STATION: FIXED\r\n",
        r"CATEGORY-OVERLAY: (YN|YL|NY)": "CATEGORY-OVERLAY: YOUTH",
        "END-OF-LOG": "END-OF-LOG:",
        "QSO ": "QSO:",
        "Programa CGLog 2.64.6": "",
        # Fix holylogger different time and date format
        r"(QSO:\s*[0-9.]+\s*[A-Z]+\s*)(\d{4})(\d{2})(\d{2})(\s*\d{4})\d{2}": r"\1\2-\3-\4\5",
        # Replace mode in QSOs from SSB to PH
        r"(QSO:\s*[0-9.]+\s*)SSB": r"\1PH",
    }
    _KEYS_TO_UPPERCASE = ["CATEGORY-ASSISTED", "CATEGORY-POWER", "CATEGORY-TRANSMITTER"]
    for key in _KEYS_TO_UPPERCASE:
        _ERROR_FIXES[f"({key}): (.*)\r\n"] = lambda match: f"{match.group(1)}: {match.group(2).upper()}\r\n"
    del key

    def __init__(self, log):
        cabrillo_data = log["log"]

        # Replace some errors with valid cabrillo
        for error, fix in self._ERROR_FIXES.items():
            cabrillo_data = re.sub(error, fix, cabrillo_data, flags=re.M)

        # Remove empty lines
        cabrillo_data = "\r\n".join(line for line in cabrillo_data.strip().split("\r\n") if len(line.strip()) > 0)

        self._cabrillo_data = cabrillo_data

        try:
            self.log = parse_log_text(cabrillo_data, ignore_unknown_key=True, ignore_order=True)
        except ValueError:
            raise InvalidLog

        self.callsign = log["callsign"].upper()
        self.name = log["name"]
        self.continent = log["continent"].upper()
        self.dxcc = log["dxcc"].capitalize()

        self.operator = self.log.category_operator or "SINGLE-OP"
        self.power = self.log.category_power or "HIGH"
        self.band = self.log.category_band or "ALL"
        self.category = log["category"].upper()


class CallsignInfo:
    _database_updated = False
    _NORMALIZED_COUNTRY_NAMES = {
        "UNITED STATES OF AMERICA": "UNITED STATES",
        "CZECH REPUBLIC": "CZECHIA",
        "SLOVAK REPUBLIC": "SLOVAKIA",
        "FEDERAL REPUBLIC OF GERMANY": "GERMANY",
    }

    def __init__(self):
        # Needs to be executed once to update the library global state
        if not CallsignInfo._database_updated:
            call_to_dxcc.book("2E", "England", "EU", 223)
            call_to_dxcc.book("2I0", "Northern Ireland", "EU", 265)
            call_to_dxcc.book("2M", "Scotland", "EU", 279)
            call_to_dxcc.book("2W0", "Wales", "EU", 294)
            call_to_dxcc.book("5P", "Denmark", "EU", 221)
            call_to_dxcc.book("9M8", "East Malaysia", "OC", 46)
            call_to_dxcc.book("9W2", "West Malaysia", "OC", 299)
            call_to_dxcc.book("AO5", "Spain", "EU", 281)
            call_to_dxcc.book("AO75", "Spain", "EU", 281)
            call_to_dxcc.book("AT", "India", "AS", 324)
            call_to_dxcc.book("CQ7", "Portugal", "EU", 272)
            call_to_dxcc.book("CR5", "Portugal", "EU", 272)
            call_to_dxcc.book("CS4", "Azores", "EU", 149)
            call_to_dxcc.book("CS7", "Portugal", "EU", 272)
            call_to_dxcc.book("DS2", "Republic of Korea", "AS", 137)
            call_to_dxcc.book("HF", "Poland", "EU", 269)
            call_to_dxcc.book("IM", "Sardinia", "EU", 225)
            call_to_dxcc.book("KG", "United States of America", "NA", 291)
            call_to_dxcc.book("KP4", "Puerto Rico", "NA", 202)
            call_to_dxcc.book("L32", "Argentina", "SA", 100)
            call_to_dxcc.book("U1", "European Russia", "EU", 54)
            call_to_dxcc.book("U3", "European Russia", "EU", 54)
            call_to_dxcc.book("VK9D", "Norfolk I.", "OC", 189)
            call_to_dxcc.book("XQ", "Chile", "SA", 112)
            call_to_dxcc.book("ZB", "Gibraltar", "EU", 233)
            CallsignInfo._database_updated = True

    @functools.lru_cache(maxsize=None)
    def _lookup_callsign(self, callsign):
        country, continent, _ = call_to_dxcc.data_for_call(callsign)
        return country, continent

    @functools.lru_cache(maxsize=None)
    def lookup(self, callsign):
        country, continent = self._lookup_callsign(callsign)
        country = country.upper()
        country = self._NORMALIZED_COUNTRY_NAMES.get(country, country)
        return country, continent

    def is_israeli(self, callsign):
        try:
            country, _ = self.lookup(callsign)
            return country == "ISRAEL"
        except call_to_dxcc.DxccUnknownException:
            return False

    def is_maritime(self, callsign):
        return callsign.upper().endswith("/MM")


INSERT_QUERY = """
INSERT IGNORE INTO wwhc_results (
active, year, callsign, name, dxcc, continent, category, qso, points, mults, score
)
 VALUES ( %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
 ON DUPLICATE KEY UPDATE
 name=VALUES(name),
 qso=VALUES(qso),
 points=VALUES(points),
 mults=VALUES(mults),
 score=VALUES(score),
 dxcc=VALUES(dxcc),
 continent=VALUES(continent),
 category=VALUES(category);
""".replace("\n", "")


class Holyland:
    JSON_CACHE_PATH = Path(tempfile.gettempdir(), "holylogs.json")

    def __init__(self, should_cache):
        self.logs = []
        self.invalid_logs = []
        for log in self._load_logs(should_cache):
            try:
                self.logs.append(Holylog(log))
            except InvalidLog:
                self.invalid_logs.append(log)

        self.logs = {log.callsign: log for log in self.logs}
        self._callsign_info = CallsignInfo()
        self.israeli_callsigns, self.foreign_callsigns = self._split_israeli_and_foreign_callsigns(self.logs)

    def _load_logs_from_remote_server(self):
        request = urllib.request.urlopen("https://iarc.org/wwhc/Server/get_all_logs.php")
        return json.load(request)

    def _load_logs(self, should_cache):
        if should_cache:
            logs = None
            try:
                with self.JSON_CACHE_PATH.open("rb") as cache_file:
                    logs = json.load(cache_file)
            except (IOError, json.JSONDecodeError):
                logs = self._load_logs_from_remote_server()
                with self.JSON_CACHE_PATH.open("w") as cache_file:
                    json.dump(logs, cache_file)
                    cache_file.flush()
            return logs
        else:
            return self._load_logs_from_remote_server()

    def _split_israeli_and_foreign_callsigns(self, logs):
        """Create 2 lists, of israeli and foreign callsigns."""
        israeli_callsigns = []
        foreign_callsigns = []
        for callsign in logs:
            if self._callsign_info.is_israeli(callsign):
                israeli_callsigns.append(callsign)
            else:
                foreign_callsigns.append(callsign)
        return israeli_callsigns, foreign_callsigns

    def frequency_to_band(self, frequency):
        """Normalize a given string frequency. This function handle a lot of weird cases."""
        frequency = float(frequency)
        if not frequency.is_integer():
            # "7.20000" -> "7000"
            frequency = frequency * 1000
        frequency = int(frequency)

        # "28" -> "28000"
        if frequency < 100:
            frequency *= 1000
        # "21242", "21.242" -> "21000"
        for name, (start, stop) in FREQ_RANGES.items():
            if start <= frequency <= stop:
                return name
        else:
            raise ValueError(f"Cannot process frequency: {frequency}")

    def process_log_by_profile(self, callsign, **profile):
        """Process log by given score-counting profile.

        The profile is dict of different types of stations and the amount of granted points.
        The profile contain the following keys: maritime, israeli, same_dxcc, same_continent, other_continent. """
        current_log = self.logs[callsign]
        report = {
            "unknown_callsign_dxcc": [],
            "duplicate_qsos": [],
            "invalid_holysquares": [],
        }
        qsos = 0
        score = 0
        holysquares = set()
        dxccs = set()
        duplicate_qso = set()
        call_list = set()

        current_dxcc, current_continent = self._callsign_info.lookup(callsign)

        for qso in current_log.log.valid_qso:
            other_callsign = qso.dx_call
            band = self.frequency_to_band(qso.freq)
            text_qso = str(qso).lstrip("QSO: ")

            try:
                other_dxcc, other_continent = self._callsign_info.lookup(other_callsign)
            except call_to_dxcc.DxccUnknownException:
                report["unknown_callsign_dxcc"].append({"qso": text_qso, "callsign": other_callsign})
                continue

            qso_id = (qso.de_call, other_callsign, band, qso.mo)
            if qso_id in duplicate_qso:
                report["duplicate_qsos"].append(text_qso)
                continue
            else:
                duplicate_qso.add(qso_id)
                qsos += 1

            if self._callsign_info.is_maritime(other_callsign):
                score += profile["maritime"]
            elif self._callsign_info.is_israeli(other_callsign):
                holysquare = qso.dx_exch[1].upper()
                if HOLYSQUARE_REGEX.match(holysquare):
                    holysquares.add((holysquare, band))
                else:
                    report["invalid_holysquares"].append({
                        "qso": text_qso,
                        "callsign": other_callsign,
                        "holysqaure": holysquare,
                    })
                    # We don't skip invalid holysquares on purpose, to count the israeli callsign and dxcc

                score += profile["israeli"]

            elif current_dxcc == other_dxcc:
                score += profile["same_dxcc"]
            elif current_continent == other_continent:
                score += profile["same_continent"]
            else:
                score += profile["other_continent"]

            dxccs.add((other_dxcc, band))
            call_list.add(other_callsign)

        multipliers = len(holysquares) + len(dxccs)
        report.update(
            dxcc=current_dxcc,
            continent=current_continent,
            qsos=qsos,
            base_score=score,
            multipliers=multipliers,
            final_score=score * multipliers,
            category=current_log.category,
            name=current_log.name,
            holysquares=list(sorted(f"{band}-{holysquare}" for holysquare, band in holysquares)),
            dxccs=list(sorted(f"{band}-{country}" for country, band in dxccs)),
            is_israeli=self._callsign_info.is_israeli(callsign),
            countries=set(list(f"{country}" for country, band in dxccs)),
            call_list=call_list
        )
        return report

    def process_log(self, callsign):
        """Process a single log of a callsign and generate a report with final score and more information."""
        if self._callsign_info.is_israeli(callsign):
            return self.process_log_by_profile(
                callsign,
                israeli=1,
                same_dxcc=None,
                same_continent=2,
                other_continent=8,
                maritime=4,
            )
        else:
            return self.process_log_by_profile(
                callsign,
                israeli=8,
                same_dxcc=1,
                same_continent=2,
                other_continent=4,
                maritime=4,
            )

    def process_all_logs(self):
        """Returns a big dict of all processes logs with calculated scores and more information."""
        import mysql.connector

        # Load configuration from config file
        config = {
            "database": {
                "host": "",
                "user": "",
                "password": "",
                "database": "",
            }
        }
        with Path(".", "modules", "db_config.json").open("r") as config_file:
            config = json.load(config_file)
        # Establish a connection to the MySQL server
        connection = mysql.connector.connect(**config['database'])
        # Create a cursor object to execute SQL queries
        cursor = connection.cursor()

        # Define your SQL query to insert data
        try:
            data_to_insert = []
            for callsign in self.logs:
                participant = self.process_log(callsign)
                # data_to_insert.append(participant)
                data_to_insert.append((
                    0,
                    current_year,
                    callsign,
                    participant["name"],
                    participant["dxcc"].title(),
                    participant["continent"],
                    participant["category"],
                    participant["qsos"],
                    participant["base_score"],
                    participant["multipliers"],
                    participant["final_score"],
                ))
            # Execute the query
            cursor.executemany(INSERT_QUERY, data_to_insert)
            # Commit the transaction
            connection.commit()
        finally:
            # Close the cursor and connection
            cursor.close()
            connection.close()
        return data_to_insert


def print_line(items):
    line = "".join(str(item).ljust(15) for item in items)
    print(line)


def print_scores(holyland, callsigns, title):
    """Pretty print a given list of callsigns in a table for debugging purposes."""
    print(f"\n===== {title} =====")
    header_row = ["#", "Callsign", "qsos", "Score", "Claimed", "Category", "dups"]
    print_line(header_row)

    total_qsos = 0
    countries = set()
    call_list = set()

    for index, callsign in enumerate(callsigns):
        report = holyland.process_log(callsign)
        countries.update(report["countries"])
        call_list.update(report["call_list"])

        claimed_score = holyland.logs[callsign].log.claimed_score or ""
        category = holyland.logs[callsign].category

        items = [
            index + 1,
            callsign,
            report["qsos"],
            report["final_score"],
            claimed_score,
            category,
            len(report["duplicate_qsos"]),
        ]
        print_line(items)
        total_qsos = total_qsos + report["qsos"]

    print(f"{total_qsos} QSOs with {len(call_list)} callsigns")
    formatted_countries = textwrap.fill(
        ", ".join(sorted(countries)),
        initial_indent=" " * 4,
        subsequent_indent=" " * 4
    )
    print(f"{len(countries)} Countries:\n{formatted_countries}")


def parse_args():
    parser = argparse.ArgumentParser(description="The holyengine calculates the holyscore of the holyland contest")
    parser.add_argument(
        "--cache",
        action="store_true",
        default=False,
        help="Cache logs json from server to local file, for development"
    )
    parser.add_argument(
        "--print-table",
        action="store_true",
        default=False,
        help="Print only final scores in pretty table, for development"
    )
    return parser.parse_args()


def main():
    args = parse_args()
    holyland = Holyland(args.cache)

    if args.print_table:
        print_scores(holyland, holyland.foreign_callsigns, "Foreign logs")
        print_scores(holyland, holyland.israeli_callsigns, "Israeli logs")
    else:
        print(json.dumps(holyland.process_all_logs()))


if __name__ == "__main__":
    main()
