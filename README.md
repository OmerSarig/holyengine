# Description
The script reads all the logs from the API iarc.org endpoint and prints a big json with all the calculated data.

# Installation and Usage
To install the script:
* Use python 3.8 or above (May work on other versions, not tested).
* Install package: `pip install cabrillo call-to-dxcc`
* Execute: `python holyengine.py`
* For development helpers execute: `python holyengine.py --help`
